<?php
    
    include_once "dados.php";

     if(!isset($_SESSION))
        session_start();
   
    $carrinho = isset($_SESSION['carrinho'])? $_SESSION['carrinho']: array();
    if(isset($_GET['id'])){
        $id = $_GET['id'];

    $exist_item = false;

    foreach ($carrinho as $i => $item){
        if($item['id'] == $id){
            $exist_item = true;
            
            $carrinho[$i]['qt']++;
        }
    }
    if(!$exist_item){
        $qt_produtos = count($todos_produtos);
    
        $i = 0;
        while($i < $qt_produtos && $todos_produtos[$i]['id'] != $id)
            $i++;  
            if($i < $qt_produtos){ 
                
                $produto = $todos_produtos[$i];
                
                $produto['qt'] = 1;
                
                $carrinho[] = $produto;
                
                
            }
        }

        $_SESSION['carrinho'] = $carrinho;

    }

    header("Location:produtos.php");
?>